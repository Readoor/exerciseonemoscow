//
//  exercise_one_for_moscowTests.swift
//  exercise-one-for-moscowTests
//
//  Created by Evgeny Ratnikov on 29.12.17.
//  Copyright © 2017 ratnikovInco. All rights reserved.
//

import XCTest
@testable import exercise_one_for_moscow

class exercise_one_for_moscowTests: XCTestCase {
    
    var userData:UsersAndEmailsData!
    var excerciseOneVC:ExerciseOneForMoscow!
    
    var firstTestDictionaryUsersEmails:[String:Set<String>] = [:]
    var secondTestDictionaryUsersEmails:[String:Set<String>] = [:]
    var thirdTestDictionaryUsersEmails:[String:Set<String>] = [:]
    
    var expectedResultFirstTestDictionary:[String:Set<String>]!
    var expectedResultSecondTestDictionary:[String:Set<String>]!
    var expectedResultThirdTestDictionary:[String:Set<String>]!
    
    var firstNameFirstInput:String!
    var firstNameSecondInput:String!
    var firstNameThirdInput:String!
    
    var constAccum:Int!
    
    override func setUp() {
        super.setUp()
        userData = UsersAndEmailsData()
        excerciseOneVC = ExerciseOneForMoscow()
        constAccum = 0
        
        firstTestDictionaryUsersEmails = userData.inputFirstTestDictionary
        secondTestDictionaryUsersEmails = userData.inputSecondTestDictionary
        thirdTestDictionaryUsersEmails = userData.inputThirdTestDictionary
        
        firstNameFirstInput = Array(firstTestDictionaryUsersEmails.keys)[constAccum]
        firstNameSecondInput = Array(secondTestDictionaryUsersEmails.keys)[constAccum]
        firstNameThirdInput = Array(thirdTestDictionaryUsersEmails.keys)[constAccum]
        
        expectedResultFirstTestDictionary = ["u2": ["j", "n", "u", "d", "1", "8", "m", "3", "e", "s", "l", "g", "y", "5"]]
        
        expectedResultSecondTestDictionary = ["u2": ["w", "34", "n", "v", "u", "x", "q", "b", "8", "r", "c", "e", "7", "y", "h", "j", "o", "k", "d", "t", "1", "i", "m", "l", "g", "5"]]
        
        expectedResultThirdTestDictionary = ["u5": ["41", "34", "w", "12", "n", "9", "v", "u", "22", "x", "45", "b", "8", "r", "c", "6", "e", "7", "44", "h", "88", "y", "j", "p", "f", "o", "k", "d", "t", "33", "2", "1", "a", "i", "m", "3", "4", "s", "g", "l", "10", "0", "5"]]
    }
    
    override func tearDown() {
        userData = nil
        excerciseOneVC = nil
        super.tearDown()
    }
    
    func testAlgorithmWithFirstInput(){
        
        excerciseOneVC.unionUsersByEmail(nextUser: firstNameFirstInput, emailsByUser: &firstTestDictionaryUsersEmails, iterationValue: constAccum)
        
        XCTAssertNotNil(firstTestDictionaryUsersEmails)
        
        XCTAssertEqual(firstTestDictionaryUsersEmails, expectedResultFirstTestDictionary)
        
        XCTAssertFalse(firstTestDictionaryUsersEmails != expectedResultFirstTestDictionary)
    }
    
    func testAlgorithmWithSecondInput(){
        
        excerciseOneVC.unionUsersByEmail(nextUser: firstNameSecondInput, emailsByUser: &secondTestDictionaryUsersEmails, iterationValue: constAccum)
        
        XCTAssertNotNil(secondTestDictionaryUsersEmails)
        
        XCTAssertEqual(secondTestDictionaryUsersEmails, expectedResultSecondTestDictionary)
        
        XCTAssertFalse(secondTestDictionaryUsersEmails != expectedResultSecondTestDictionary)
    }
    
    func testAlgorithmWithThirdInput(){
        
        excerciseOneVC.unionUsersByEmail(nextUser: firstNameThirdInput, emailsByUser: &thirdTestDictionaryUsersEmails, iterationValue: constAccum)
        
        XCTAssertNotNil(thirdTestDictionaryUsersEmails)
        
        XCTAssertEqual(thirdTestDictionaryUsersEmails, expectedResultThirdTestDictionary)
        
        XCTAssertFalse(thirdTestDictionaryUsersEmails != expectedResultThirdTestDictionary)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            
            //              excerciseOneVC.unionUsersByEmail(nextUser: firstNameFirstInput, emailsByUser: &firstTestDictionaryUsersEmails, iterationValue: constAccum)
            
            //            excerciseOneVC.unionUsersByEmail(nextUser: firstNameSecondInput, emailsByUser: &secondTestDictionaryUsersEmails, iterationValue: constAccum)
            
            excerciseOneVC.unionUsersByEmail(nextUser: firstNameThirdInput, emailsByUser: &thirdTestDictionaryUsersEmails, iterationValue: constAccum)
            
        }
    }
    
}
