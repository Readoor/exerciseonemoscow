//
//  ExerciseOneForMoscow.swift
//  exercise-one-for-moscow
//
//  Created by Evgeny Ratnikov on 29.12.17.
//  Copyright © 2017 ratnikovInco. All rights reserved.
//

import Foundation

class ExerciseOneForMoscow{
    
    //Алгоритм обьединения пользователей по одинаковым элементам в множестве, где nextUser - первый ключ к первому множеству, emailsByUser - словарь неупорядочных множеств, iterationValue - константа начала цикла(аккумулятор)
    func unionUsersByEmail(nextUser:String, emailsByUser: inout [String:Set<String>], iterationValue: Int){
        //Присваивание константе emailsByUserLength значения количества элементов в словаре emailsByUser
        let emailsByUserLength:Int = emailsByUser.count
        //Начало цикла по ключу словаря и его значения в словаре emailsByUser
        for (user, emails) in emailsByUser{
            //Опциональная проверка множества по ключу К в словаре emailsByUser
            if let emailsOfFirstUser = emailsByUser[nextUser]{
                //Определения общих значений между множеством emailsOfFirstUser и emails
                if !emailsOfFirstUser.isDisjoint(with: emails) {
                    //Проверка на равность
                    if emailsOfFirstUser != emails {
                        //обьединение множества emailsOfFirstUser[nextUser] с множеством emails
                        emailsByUser[nextUser] = emailsOfFirstUser.union(emails)
                        //Удаление множества emails по ключу user
                        emailsByUser.removeValue(forKey: user)
                    }
                }
            }
        }
        //Проверка на различия количества элементов в словаре на начало и конец цикла. Запуск алгоритма повторно, если значение изменилось, для обьединения всех возможных элементов после обьединения множеств в первый раз(Хвостовая рекурсия)
        if emailsByUserLength > emailsByUser.count {
            unionUsersByEmail(nextUser: nextUser, emailsByUser: &emailsByUser, iterationValue: iterationValue)
        }else{
            //Проверка границы массива с заданным значением константы
            if emailsByUser.count > iterationValue + 1 {
                //Запуск алгоритма с другим значением константы, для проверки следующего элемента в словаре
                unionUsersByEmail(nextUser: Array(emailsByUser.keys)[iterationValue + 1], emailsByUser: &emailsByUser, iterationValue: iterationValue + 1)
            }
        }
    }
}
